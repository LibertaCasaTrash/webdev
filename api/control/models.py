from django.db import models

class ScheduledTask(models.Model):
    from control.tasks import REGISTERED_TASKS

    class CatchupMode(models.IntegerChoices):
        SKIP = 0, ('SKIP TO LATEST')
        EXEC = 1, ('EXECUTE ALL')

    class Interval(models.IntegerChoices):
        HOURS = 1, ('HOURS')
        DAYS = 24, ('DAYS')
        WEEKS = 168, ('WEEKS')

    active = models.BooleanField(default=False)
    name = models.CharField(max_length=128)
    action = models.CharField(max_length=128, choices=zip(
        REGISTERED_TASKS.keys(), REGISTERED_TASKS.keys()))
    kwargs = models.JSONField(blank=True, null=True)
    repeat = models.IntegerField('Repeat Every', default=0)
    repeat_interval = models.IntegerField(
        'Interval', choices=Interval.choices, default=1)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField(blank=True, null=True)
    next_cycle = models.DateTimeField(blank=True, null=True)
    catchup_mode = models.IntegerField(
        default=0, choices=CatchupMode.choices)

    class Meta():
        verbose_name = 'Scheduled Task'
        verbose_name_plural = 'Scheduled Tasks'

    def __str__(self):
        return self.name
