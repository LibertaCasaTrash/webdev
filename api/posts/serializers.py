from django.contrib.auth.models import User
from .models import *

from rest_framework import serializers

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        # better to explicitly mention the fields
        fields = '__all__'