from django.contrib.auth.models import User
from django.shortcuts import render
from snippets.serializers import UserSerializer
from .models import Post
from .serializers import PostSerializer
from posts.permissions import IsOwnerOrReadOnly
from rest_framework import permissions, renderers
from rest_framework.reverse import reverse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

class PostsViewSet(viewsets.ModelViewSet):
    """
    docstring
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                         IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `retrieve` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer